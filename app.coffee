# Init Library

express = require 'express'
Pipa 	= require 'pipa'
app 	= express()

# Express Setup 

ejs = require('ais-ejs-mate')({ open: '{{', close:'}}'})
app.engine '.html', ejs

app.set 'views', __dirname + '/public' 
app.set 'view engine', 'html'

# Port

port = 1234

pipa = new Pipa app,'router','controller'
pipa.open()

app.listen port, ()->
	console.log ""
	console.log "==============================="
	console.log "Welcome to the Blabla"
	console.log "Server running on localhost:" + port
	console.log "==============================="
